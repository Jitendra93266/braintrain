package com.getmeanapp.braintrain;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BrainTrainApp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brain_train_app);
    }
}
